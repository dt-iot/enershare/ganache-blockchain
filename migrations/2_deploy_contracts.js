/** DOCKER DEPLOYMENT */
const fs = require("fs");
const os = require("os");

const env_file = ".env"; // /app/.env
//const env_file = "/app/.env"; // /app/.env
//const file_blank = "/app/env/.env"; // /app/env/.env
//const env_file_volume = "/env/.env"; // volume

//if (fs.existsSync(env_file_volume)) {
//      console.log("file .env exist at: " + env_file_volume);
//      fs.copyFileSync(env_file_volume, env_file);
//      console.log("file .env COPIED from " + env_file_volume + " to " + env_file);
//} else {
//      console.log("file .env NOT exist at: " + env_file_volume);
//      fs.copyFileSync(file_blank, env_file_volume);
//      fs.copyFileSync(file_blank, env_file);
//      console.log("file .env COPIED from " + file_blank + " to " + env_file_volume);
//      console.log("file .env COPIED from " + file_blank + " to " + env_file);
//}

require('dotenv').config();

console.log("process.env.VITE_ENERSHARE_TOKEN:", process.env.VITE_ENERSHARE_TOKEN);
console.log("process.env.VITE_TRANSACTION_PROCESSOR:", process.env.VITE_TRANSACTION_PROCESSOR);

if( (process.env.VITE_ENERSHARE_TOKEN == undefined || process.env.VITE_ENERSHARE_TOKEN == "") || (process.env.VITE_TRANSACTION_PROCESSOR == undefined || process.env.VITE_TRANSACTION_PROCESSOR == "")){
      var token=artifacts.require("EnershareToken.sol");
      var TransactionProcessor=artifacts.require("TransactionProcessor.sol");

      module.exports = function (deployer) {
            // deploy Contract2 first
            //if(process.env.VITE_ENERSHARE_TOKEN == undefined && process.env.VITE_TRANSACTION_PROCESSOR == undefined){
                  deployer.deploy(token).then(async () => {
                        // get JS instance of deployed contract
                        const tokenInstance = await token.deployed(); 
                        // pass its address as argument for Contract1's constructor
                        console.log("token ADDRESS: ", tokenInstance.address);
                        //setEnvValue("VITE_ENERSHARE_TOKEN", tokenInstance.address, env_file_volume);
                        setEnvValue("VITE_ENERSHARE_TOKEN", tokenInstance.address, env_file);
                        await deployer.deploy(TransactionProcessor, tokenInstance.address); 
                        const TransactionProcessorInstance = await TransactionProcessor.deployed(); 
                        console.log("TransactionProcessorInstance ADDRESS: ", TransactionProcessorInstance.address);
                        //setEnvValue("VITE_TRANSACTION_PROCESSOR", TransactionProcessorInstance.address, env_file_volume);
                        setEnvValue("VITE_TRANSACTION_PROCESSOR", TransactionProcessorInstance.address, env_file);
                  });
            //}
            
      };

} else {
      module.exports = function (deployer) {
            return false;
      };
}

function setEnvValue(key, value, env_file) {

      // read file from hdd & split if from a linebreak to a array
      const ENV_VARS = fs.readFileSync(env_file, "utf8").split(os.EOL);
  
      // find the env we want based on the key
      const target = ENV_VARS.indexOf(ENV_VARS.find((line) => {
          return line.match(new RegExp(key));
      }));
  
      // replace the key/value with the new value
      ENV_VARS.splice(target, 1, `${key}=${value}`);
  
      // write everything back to the file system
      fs.writeFileSync(env_file, ENV_VARS.join(os.EOL));
  
}


/* LOCAL DEPLOYMENT
const fs = require("fs");
const os = require("os");

const env_file = ".env"; // /app/.env

require('dotenv').config();

console.log("process.env.ENERSHARE_TOKEN_SCA:", process.env.ENERSHARE_TOKEN_SCA);
console.log("process.env.TRANSACTIONPROCESSOR_SCA:", process.env.TRANSACTIONPROCESSOR_SCA);

if( (process.env.ENERSHARE_TOKEN_SCA == undefined || process.env.ENERSHARE_TOKEN_SCA == "") || (process.env.TRANSACTIONPROCESSOR_SCA == undefined || process.env.TRANSACTIONPROCESSOR_SCA == "")){
      var EnershreToken=artifacts.require("EnershareToken.sol");
      var TransactionProcessor=artifacts.require("TransactionProcessor.sol");

      module.exports = function (deployer) {
            // deploy Contract2 first
            //if(process.env.BD4NRG_SCA == undefined && process.env.TRANSACTIONPROCESSOR_SCA == undefined){
                  deployer.deploy(EnershreToken).then(async () => {
                        // get JS instance of deployed contract
                        const EnershreTokenInstance = await EnershreToken.deployed(); 
                        // pass its address as argument for Contract1's constructor
                        console.log("ENERSHARE_TOKEN ADDRESS: ", EnershreTokenInstance.address);
                        setEnvValue("ENERSHARE_TOKEN_SCA", EnershreTokenInstance.address, env_file);
                        await deployer.deploy(TransactionProcessor, EnershreTokenInstance.address); 
                        const TransactionProcessorInstance = await TransactionProcessor.deployed(); 
                        console.log("TransactionProcessor ADDRESS: ", TransactionProcessorInstance.address);
                        setEnvValue("TRANSACTIONPROCESSOR_SCA", TransactionProcessorInstance.address, env_file);
                  });
            //}
            
      };

} else {
      module.exports = function (deployer) {
            return false;
      };
}

function setEnvValue(key, value, env_file) {

      // read file from hdd & split if from a linebreak to a array
      const ENV_VARS = fs.readFileSync(env_file, "utf8").split(os.EOL);
  
      // find the env we want based on the key
      const target = ENV_VARS.indexOf(ENV_VARS.find((line) => {
          return line.match(new RegExp(key));
      }));
  
      // replace the key/value with the new value
      ENV_VARS.splice(target, 1, `${key}=${value}`);
  
      // write everything back to the file system
      fs.writeFileSync(env_file, ENV_VARS.join(os.EOL));
  
}

*/