/**
 * @author Lorenzo Cristofori <lorenzo.cristofori@eng.it>
*/

const path = require('path');
const fs = require('fs');
const {signedTransactionMethod} = require('./utils.js')

require('dotenv').config();
const { VITE_CHAINID, VITE_BLOCKCHAIN_URL, VITE_BLOCKCHAIN_PORT, VITE_OWNER_PUBLIC, VITE_OWNER_PRIVATE, VITE_ENERSHARE_TOKEN,  VITE_TRANSACTION_PROCESSOR } = process.env;

const Web3 = require('web3');
//provide the link of the blockchain node
console.log("************* BLOCKCHAIN:", VITE_BLOCKCHAIN_URL + ":" + VITE_BLOCKCHAIN_PORT);
//const web3 = new Web3("http://0.0.0.0:7545");
const web3 = new Web3("http://" + VITE_BLOCKCHAIN_URL + ":" + VITE_BLOCKCHAIN_PORT);
var account = web3.eth.accounts;

const TransactionProcessorContractJsonPath = path.resolve(__dirname, 'build/contracts/TransactionProcessor.json');
const TransactionProcessorContractJson = JSON.parse(fs.readFileSync(TransactionProcessorContractJsonPath));
const TransactionProcessorSmartContractABI = TransactionProcessorContractJson.abi;

//provide smart contract address
const TransactionProcessorContract = new web3.eth.Contract(TransactionProcessorSmartContractABI, VITE_TRANSACTION_PROCESSOR);

// The buyer calls the smart contract to register the purchase
async function initializer(){
    let initializerMethod = await TransactionProcessorContract.methods.initializer(VITE_ENERSHARE_TOKEN).encodeABI();
    try {
        signedTransactionMethod(account, initializerMethod, VITE_TRANSACTION_PROCESSOR, VITE_OWNER_PRIVATE, web3, VITE_CHAINID, []).then(res => {
            console.log(res);
        }).catch(err => {
            console.log(err);
        })
    } catch (error) {
        return;
    }
}

initializer();