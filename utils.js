exports.signedTransactionMethod = function(account, methodABI, SmartContractAddress, PRIVATE_KEY, web3, CHAINID, formatObjects){

    return new Promise(function (resolve, reject) {
        
        account.signTransaction({
            gas: 4465030,
            to: SmartContractAddress,
            data: methodABI,
            value: "0x00",
            chainId: CHAINID // Commenting out this line results in an invalid rawTransaction
        }, PRIVATE_KEY)
        .then(signedTx => {
                web3.eth.sendSignedTransaction(signedTx.rawTransaction).then(receipt => {

                //var data_splitted = divideInBytes32(receipt.logs[0].data);
                //var data_composed = recompose_data(data_splitted, formatObjects);
                //var result = web3.eth.abi.decodeParameters(formatObjects, data_composed);
                //var result = web3.eth.abi.decodeLog(formatObject, data_composed, receipt.logs[0].topics);

                resolve(receipt);
    
            }).catch(error => {
                reject(error);
            })

        }).catch(error => {
            reject(error);
        });
        
    });
    
}