// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20; // >=0.8.12 <0.9.0; // to support string.concat()

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
// import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Permit.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

/// @title The Enershare Token
/// @author Alerox, 2024 (0x7efC9d38581c7Cb8372325c349Ec5095D027B3Dd)
/// @dev Used inside the Enershare EU cofunded project (Grant Agreement No 101069831).
/// @custom:dev-run-script scripts/deploy_ETK_with_ethers.ts
contract EnershareToken is
    ERC20,
    ERC20Burnable,
    // ERC20Permit,
    Ownable
{
    /// Percentage (in ‰) applied to token purchase (range: 0.0-25.5%, default: 0.0%).
    uint8 public constant buyFee = 0;
    /// Percentage (in ‰) applied to token withdraw (range: 0.0-25.5%, default: 2.5%).
    uint8 public constant sellFee = 25;
    /// Percentage (in ‰) applied to token transfer (range: 0.0-25.5%, default: 0.1%).
    uint8 public constant transferFee = 1;

    event Buy(address indexed by, uint256 value);
    event Sell(address indexed by, uint256 value);

    /// The contract constructor. The contract deployer is the initial contract owner. 
    /// 1 𑩅 ("Enershare token") is equivalent to 1000000000000000 ΞTK ("token units").
    /// Analogous to normal ether, where 1 Ξ is equivalent to 1000000000000000000 wei.
    constructor()
        ERC20(unicode"EnershareToken (𑩅)", unicode"ΞTK")
        // ERC20Permit(unicode"EnershareToken (𑩅)")
        Ownable(msg.sender)
    {}

    /// Send ethers to buy tokens, send 0 to sell them all.
    receive() external payable {
        if (msg.value > 0) buy();
        else sellBalance();
    }

    /// Returns the tokens owned by the caller.
    /// @return The amount (in ΞTK)
    function balance() external view returns (uint256) {
        return balanceOf(msg.sender);
    }

    /// Returns the ethers owned by the contract.
    /// @return The amount (in wei)
    function reserve() external view returns (uint256) {
        return walletOf(address(this));
    }

    /// Returns the ethers owned by the caller.
    /// @return The amount (in wei)
    function wallet() external view returns (uint256) {
        return walletOf(msg.sender);
    }

    /// Returns the decimal representation of the tokens owned by the caller. 
    /// @dev Can be used for display purposes or casted to a floating-point number.
    /// @return The string corresponding to the amount (in 𑩅)
    function balanceInTokens() external view returns (string memory) {
        return balanceInTokensOf(msg.sender);
    }

    /// Returns the decimal representation of the ethers owned by the contract. 
    /// @dev Can be used for display purposes or casted to a floating-point number.
    /// @return The string corresponding to the amount (in Ξ)
    function reserveInEthers() external view returns (string memory) {
        return walletInEthersOf(address(this));
    }

    /// Returns the decimal representation of the ethers owned by the caller. 
    /// @dev Can be used for display purposes or casted to a floating-point number.
    /// @return The string corresponding to the decimal amount (in Ξ)
    function walletInEthers() external view returns (string memory) {
        return walletInEthersOf(msg.sender);
    }

 // /// Returns the number of token units owned by `account`.
 // /// @param account The address of the account
 // /// @return The amount (in ΞTK)
 // function balanceOf(address account) public view returns (uint256) {...} ; // from ERC20

    /// Returns the number of ethers owned by `account`.
    /// @param account The address of the account
    /// @return The amount (in wei)
    function walletOf(address account) public view returns (uint256) {
        return account.balance;
    }

    /// Returns the decimal representation of the tokens owned by `account`. 
    /// @dev Can be used for display purposes or casted to a floating-point number.
    /// @param account The address of the account
    /// @return The string corresponding to the decimal amount (in 𑩅)
    function balanceInTokensOf(address account) public view returns (string memory) {
        return toString(balanceOf(account), decimals());
    }

    /// Returns the decimal representation of the ethers owned by `account`.
    /// @dev Can be used for display purposes or casted to a floating-point number.
    /// @param account The address of the account
    /// @return The string corresponding to the decimal amount (in Ξ)
    function walletInEthersOf(address account) public view returns (string memory) {
        return toString(walletOf(account), 18);
    }

    function decimals() public pure override returns (uint8) {
        return 15; // 1 𑩅 = 1000000000000000 ΞTK (minimum denomination = 1000 ΞTK)
    }

    /// Decimal representation of the number m * 10^(-e), with m=`mantissa`, e=`exponent`.
    /// @dev Can be used for display purposes or casted to a floating-point number.
    /// @param mantissa Significant digits of the number (e.g., 0.0123 -> 123)
    /// @param exponent Number of decimal places (e.g., 0.0123 -> 4)
    /// @return The string corresponding to the decimal number
    function toString(uint256 mantissa, uint8 exponent) public pure returns (string memory)
    {
        uint256 tenPower = 10**exponent;
        uint256 integerPart = mantissa / tenPower;
        uint256 fractionalPart = mantissa % tenPower;
        while (fractionalPart % 10 == 0 && exponent > 0) {
            fractionalPart /= 10;
            exponent--;
        }
        string memory fractionalPartStr = Strings.toString(fractionalPart);
        if (exponent > 0) {
            while (bytes(fractionalPartStr).length < exponent)
                fractionalPartStr = string.concat("0", fractionalPartStr);
            return
                string.concat(
                    Strings.toString(integerPart),
                    ".",
                    fractionalPartStr
                );
        } else {
            return string.concat(Strings.toString(integerPart));
        }
    }

    /*******************
    / EXCHANGE FUNCTIONS
    /******************/
    // Default policy to exchange tokens, in a fully decentralized way.

    /// Returns the cost of `n` tokens at fixed rate (1 𑩅 = 0.001 Ξ + `buyFee` ‰).
    /// @dev Can be used, e.g., together with buy() to purchase an integer number of tokens.
    /// @param n Integer number of tokens (in 𑩅)
    /// @return The cost (in wei)
    function costOfTokens(uint128 n) external pure returns (uint256) {
        return (n * 10**decimals() * (1000 + buyFee)) / 1000;
    }

    /// Sell `n` tokens at fixed rate (1 𑩅 = 0.001 Ξ - `sellFee` ‰). 
    /// @dev Emits a {Sell} event.
    /// @param n Integer number of tokens (in 𑩅)
    function sellTokens(uint256 n) external {
        sell(n * 10**decimals());
    }

    /// Transfer `n` tokens from the caller's account to `account`.
    /// @dev Emits a {Transfer} event.
    /// @param n Integer number of tokens to transfer (in 𑩅)
    /// @return A boolean value indicating whether the operation succeeded
    function transferTokens(address account, uint128 n) external returns (bool) {
        return transfer(account, n * 10**decimals());
    }

    /// Returns the cost of `value` token units at fixed rate (1 𑩅 = 0.001 Ξ + `buyFee` ‰).
    /// @dev Can be used, e.g., together with buy() to purchase tokens.
    /// @param value Amount of tokens (in ΞTK)
    /// @return The cost (in wei)
    function costOf(uint128 value) external pure returns (uint256) {
        return (value * (1000 + buyFee)) / 1000;
    }

    /// Buy tokens at fixed rate (1 𑩅 = 0.001 Ξ + `buyFee` ‰). 
    /// @dev Emits a {Buy} event.
    function buy() public payable {
        require(
            msg.value % (1000 + buyFee) == 0,
            string.concat(
                "Ether sent for purchase (in Wei) must be a multiple of ",
                Strings.toString(1000 + buyFee)
            )
        );
        uint256 tokens = (msg.value * 1000) / (1000 + buyFee);
        uint256 fee = msg.value - tokens;
        _mint(msg.sender, tokens);
        payable(owner()).transfer(fee); // fees sent to the owner
        // payable(owner()).transfer(fee + (sellFee * tokens) / 1000); // all fees sent (*)
        emit Buy(msg.sender, tokens);
    }

    /// Sell all tokens owned by the caller at fixed rate (1 𑩅 = 0.001 Ξ - `sellFee` ‰).
    /// @dev Emits a {Sell} event.
    function sellBalance() public {
        sell(balanceOf(msg.sender));
    }

    /// Sell tokens owned by the caller at fixed rate (1 𑩅 = 0.001 Ξ - `sellFee` ‰).
    /// @dev Emits a {Sell} event.
    /// @param value Amount of tokens (in ΞTK)
    function sell(uint256 value) public {
        require(
            value % 1000 == 0,
            unicode"Tokens to withdraw (in ΞTK) must be multiple of 1000"
        );
        uint256 fee = (value * sellFee) / 1000;
        burn(value);
        payable(msg.sender).transfer(value - fee); // exchange sent to the requester
        payable(owner()).transfer(fee); // fees sent to the owner (*) remove if already sent
        emit Sell(msg.sender, value);
    }

    function transfer(address to, uint256 value) public override returns (bool) {
        require(
            value % 1000 == 0,
            unicode"Tokens to transfer (in ΞTK) must be multiple of 1000"
        ); // for safe divisions
        return
            super.transfer(to, value - (value * transferFee / 1000 )) &&
            super.transfer(owner(), value * transferFee / 1000);
    }

    /************************************
    / [OPTIONAL] ADMINISTRATIVE FUNCTIONS
    /***********************************/
    // For custom policy to exchange tokens with a centralized trusted admin (the owner).

    /// The owner can deposit funds into the contract.
    /// @dev Can be used, e.g., to increase the reserve.
    fallback() external payable onlyOwner {}

    /// The owner can send `amount` of ethers (in wei) from the contract to `account`.
    /// @dev Can be used, e.g., for token sell reimbursement, decrease the reserve, take fees.
    function withdrawTo(address account, uint256 amount) public onlyOwner {
        payable(account).transfer(amount);
    }

    /// The owner can burn `value` of tokens (in ΞTK) from `account` without allowance.
    /// @dev Can be used, e.g., if sold off-chain, or along with {mint} for transfer refunds.
    function burnFrom(address account, uint256 value) public override {
        if (msg.sender == owner()) _burn(account, value);
        else super.burnFrom(account, value);
    }

    /// The owner can mint `value` of tokens (in ΞTK) sending them to `account`.
    /// @dev Can be used, e.g., if bought off-chain, or with {burnFrom} for transfer refunds.
    function mint(address account, uint256 value) public onlyOwner {
        _mint(account, value);
    }

    /// @dev Avoids an inherited behaviour which would lead to irreverible loss of ownership:
    /// https://docs.openzeppelin.com/contracts/2.x/api/ownership#Ownable-renounceOwnership--
    function renounceOwnership() public view override onlyOwner {
        revert("Not allowed, ownership renounce disabled");
    }

    /********************
    / INHERITED FUNCTIONS
    /*******************/

    // event Transfer(address indexed from, address indexed to, uint256 value);                // from IERC20
    // event Approval(address indexed owner, address indexed spender, uint256 value);          // from IERC20
    // event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);    // from IERC20
    // ------------------------------------------------------------------------------------------------------
    // function totalSupply() external view returns (uint256);                                 // from IERC20
    // function transfer(address to, uint256 value) external returns (bool);                   // from IERC20
    // function allowance(address owner, address spender) external view returns (uint256);     // from IERC20
    // function approve(address spender, uint256 value) external returns (bool);               // from IERC20
    // function transferFrom(address from, address to, uint256 value) external returns (bool); // from IERC20

    // function balanceOf(address account) public view virtual returns (uint256);              // from ERC20
    // function name() public view virtual returns (string memory);                            // from ERC20
    // function symbol() public view virtual returns (string memory);                          // from ERC20
    // function decimals() public view virtual returns (uint8);                                // from ERC20

    // function burn(uint256 value) public virtual;                                            // from ERC20Burnable
    // function burnFrom(address account, uint256 value) public virtual;                       // from ERC20Burnable

    // function owner() public view virtual returns (address);                                 // from Ownable
    // function renounceOwnership() public virtual onlyOwner;                                  // from Ownable
    // function transferOwnership(address newOwner) public virtual onlyOwner;                  // from Ownable
}

/***********
/ REFERENCES
/**********/

// OpenZeppelin's libraries:
// https://github.com/OpenZeppelin/openzeppelin-contracts/tree/master/contracts/

// Style Guide — Solidity documentation:
// https://docs.soliditylang.org/en/latest/style-guide.html