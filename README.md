# Ganache Blockchain Dockerized

## Getting Started

### Prerequisites

```
* Node 20+
```

### Installing

- Check out the code from this repository

```
 git clone https://gitlab.com/dt-iot/enershare/ganache-blockchain.git
```

- Move into the root directory of the application

```
cd ganache-blockchain
```

- Run the following commands

```
npm install
npm install -g truffle
```

### Configuring

Here is the list of the environment variables configurable in the .env file in the root of the project.

| VARIABLE NAME                  | DESCRIPTION                                                       | DEFAULT VALUE                                                    |
| ------------------------------ | ----------------------------------------------------------------- | ---------------------------------------------------------------- |
| **VITE_ENERSHARE_TOKEN**       | The address of the deployed Enershare Token smart contract        |                                                                  |
| **VITE_TRANSACTION_PROCESSOR** | The address of the deployed Transactions processor smart contract |                                                                  |
| **VITE_OWNER_PUBLIC**          | The public key of the owner of the contracts                      | 0x89a47029b7168a240BD49bd446F79cee6cad11D4                       |
| **VITE_OWNER_PRIVATE**         | The private key of the owner of the contracts                     | 18bad27b0ce67b140d49667e030cdb5067c362390a93d8ddae34fd2b436c9f64 |
| **VITE_BLOCKCHAIN_URL**        | The url of a blockchain                                           | 0.0.0.0                                                          |
| **VITE_BLOCKCHAIN_PORT**       | The port of the blockchain provider                               | 7545                                                             |

### Installing the Blockchain

Before Deploying the contracts you need to have a blockchain up and running.

To install the blockchain you can run:

```
docker compose up -d
```

### deploy the contracts

to deploy the contracts run:

```
truffle migrate --network development
```

if everything is okay the two contracts address can be found inside the .env file.

### Initialize Transactions Processor Smart Contract

to initialize transactions processor smart contract:

```
node InitializeTPcontract.js
```
